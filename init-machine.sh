#!/usr/bin/env zsh

GREEN=`tput setaf 2`
RED=`tput setaf 1`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RST=`tput sgr0`

CMD="$0"
APIKEY=`cat ./.htb-key`
CONNECTION_NAME="Hack the Box"

# Print an error message to stderr
function error {
	echo "[${RED}error${RST}] $@" 1>&2
	exit 1
}

# Print a warning message to stderr
function warning {
	echo "[${YELLOW}warning${RST}] $@" 1>&2
}

# Print some info to stdout
function info {
	echo "[${BLUE}info${RST}] $@"
}

# Print usage information
function print_usage {
	echo "usage: $CMD [-h] machine_id"
}

RUN_TCP=1
RUN_UDP=0
ALL_PORTS=0

while getopts ":htua" opt; do
	case ${opt} in
		t )
			RUN_TCP=1
			;;
		u )
			RUN_UDP=1
			;;
		a )
			ALL_PORTS=1
			;;
		h ) print_usage ;;
		\? )
			print_usage
			exit 1
			;;
	esac
done

# Shift out short options
shift $((OPTIND-1))

# Ensure we can or are connected to the hack the box VPN
nmcli c s --active | grep "$CONNECTION_NAME" >/dev/null || (nmcli connection up "$CONNECTION_NAME" || error "could not connect to vpn: $CONNECTION_NAM")

# Get the machine information
machine_info=`HTB_API_KEY=$APIKEY hackthebox.py get machine $1 2>/dev/null || echo -n no`
if [ "$machine_info" = "no" ]; then
	error "invalid machine id: $1"
fi

NAME=`echo -n $machine_info | grep -e "^NAME: " | cut -d' ' -f2 | tr '[:upper:]' '[:lower:]' | tr -d '\n'`
ADDRESS=`echo -n $machine_info | grep -e "^IP: " | cut -d' ' -f2 | tr -d '\n'`
HOSTNAME="$NAME.htb"

info "setting up htb environment for: $NAME"
warning "you may be prompted for sudo password (for installing /etc/hosts)"

# Install hostname in /etc/hosts
info "installing $HOSTNAME in /etc/hosts"
hosts_line=`echo "# Hack the Box - $NAME"; echo -e "$ADDRESS\t$HOSTNAME"`
sudo sh -c "cat - >>/etc/hosts"<<END
# Hack the Box - $NAME
$ADDRESS	$HOSTNAME
END

# Creating common directory tree
info "setting up directory tree"
mkdir -p "./$NAME/scans"
mkdir -p "./$NAME/artifacts"
cat <<END >"./$NAME/README.md"
# Hack the Box - $NAME - $ADDRESS

This machine has been added to /etc/hosts as $HOSTNAME. Basic nmap scans are stored in [./scans](./scans).
END

# Enter the directory tree
pushd "./$NAME"

# Run TCP scans if requested
if [ "$RUN_TCP" -eq "1" ]; then
	info "performing basic port scan (top 1000 w/ version scanning)"
	nmap -sV -oA ./scans/basic $HOSTNAME

	if [ "$ALL_PORTS" -eq "1" ]; then
		info "performing in depth port scan (all tcp ports w/ version scanning)"
		nmap -sV -p- -oA ./scans/all-ports $HOSTNAME
	fi
fi

# Run UDP scans if requested
if [ "$RUN_UDP" -eq "1" ]; then
	info "performing udp port scan (top 1000 w/ version scanning)"
	nmap -sU -sV -oA ./scans/basic-udp $HOSTNAME

	if [ "$ALL_PORTS" -eq "1" ]; then
		info "performing in depth udp scan (all udp ports w/ version scanning)"
		nmap -sV -sU -p- -oA ./scans/udp-all-ports $HOSTNAME
	fi
fi

# Go back
popd
